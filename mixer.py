#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import paho.mqtt.client as paho

import client
import watchdog

def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_message(client, userdata, msg):
    # print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
    global S1, S2, S3, S4, S5, S6, S7, s
    wd.reset()
    if msg.topic == "Switch1": S1 = (int(msg.payload))
    if msg.topic == "Switch2": S2 = (int(msg.payload))
    if msg.topic == "Switch3": S3 = (int(msg.payload))
    if msg.topic == "Switch4": S4 = (int(msg.payload))
    if msg.topic == "Switch5": S5 = (int(msg.payload))
    if msg.topic == "Switch6": S6 = (int(msg.payload))
    if msg.topic == "Switch7": S7 = (int(msg.payload))

    s = (str(S1) + " " + str(S2) + " " + str(S3) + " " + str(S4) + " " + str(S5) + " " + str(S6) + " " + str(S7))
    print("on message", s)
    c.to_send = s



def on_disconnect(client, userdata, rc):
    global S1, S2, S3, S4, S5, S6, S7, s
    S1, S2, S3, S4, S5, S6, S7 = 0, 0, 0, 0, 0, 0, 0
    s = (str(S1) + " " + str(S2) + " " + str(S3) + " " + str(S4) + " " + str(S5) + " " + str(S6) + " " + str(S7))
    print("MQTT client disconnected - ", rc)
    print(s)
    c.to_send = s


def on_message_c(c):
    pass


def on_newdata(c):
    client.publish("Off", c.received)
    print("new data resivd")


def sender(c):
    pass


def on_timeout():
    # при спрацюванні ватчдога
    global S1, S2, S3, S4, S5, S6, S7, s
    S1, S2, S3, S4, S5, S6, S7 = 0, 0, 0, 0, 0, 0, 0
    s = (str(S1) + " " + str(S2) + " " + str(S3) + " " + str(S4) + " " + str(S5) + " " + str(S6) + " " + str(S7))
    print("MQTT timeout- no data from broker")
    print(s)
    c.to_send = s


c = client.Client("mixer")
c.run()
c.on_message = on_message_c
c.on_newmsg = on_newdata
c.send_handler = sender

wd = watchdog.Watchdog(10, on_timeout)


S1 = ""
S2 = ""
S3 = ""
S4 = ""
S5 = ""
S6 = ""
S7 = ""
s = ""
old_s = ""
client = paho.Client()
client.on_subscribe = on_subscribe
client.on_message = on_message
client.on_disconnect = on_disconnect
client.connect("127.0.0.1", 1883)
wd.start()
client.publish("Off", "6")
client.subscribe(
    [("Switch1/#", 1), ("Switch2/#", 1), ("Switch3/#", 1), ("Switch4/#", 1),
     ("Switch5/#", 1), ("Switch6/#", 1), ("Switch7/#", 1)])


client.loop_forever()
