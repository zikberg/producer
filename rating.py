#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Catch out the last speaker and calculate rating by speak time """
from threading import Thread
import time

import client


def on_message(c):
    pass


def on_newdata(c):
    global is_active, gpio
    prepare = c.received.replace(',', '')
    print(len(prepare))
    print(prepare)
    if len(prepare) == 20:
        is_active = prepare[0:8].split()
        is_active = list(map(int, is_active))
        gpio = prepare[7::2]

    is_active = list(map(int, is_active))
    is_active = check_active(is_active, gpio)

    print("new data received", is_active, gpio)


def sender(c):
    c.to_send = rating


c = client.Client("rating")
c.run()
c.on_message = on_message
c.on_newmsg = on_newdata
c.send_handler = sender

TIMER_RESOLUTION = 0.2
last_speaker = 0

rating = "0 0 0 0 0"
gpio = "000001"

is_active = [0, 0, 0, 0]
olds = [0, 0, 0, 0]

start_time = dict.fromkeys(range(4))  # час початку розмови
total_time = {a: 0 for a in range(4)}  # загальна тривалість розмови
last_time = {a: 0 for a in range(4)}  # тривалість поперельньої розмови
flag_timer = dict.fromkeys(range(4))  # прапорець запуску потоку таймера розмови


def check_active(is_active, gpio):  # clearing inactive mic
    active = list(map(int, gpio[:-2]))
    b = list(map(lambda y, x: y & x, active, is_active))
    return b


def speaking_thread(mic_number):  # потік визначає тривалість розмови
    flag_timer[mic_number] = 1

    while int(is_active[mic_number]):
        time.sleep(TIMER_RESOLUTION)
        total_time[mic_number] += TIMER_RESOLUTION
        total_time[mic_number] = round(total_time[mic_number], 1)

    print('end speak', mic_number, is_active[mic_number])
    flag_timer[mic_number] = 0
    pass


def check_lastXOR(is_active, olds):  # find new speaker by XOR AND
    try:

        b = list(map(lambda y, x: ((y ^ x) & x) & x, olds, is_active))

        # print('last -----', b)
        last_speaker = b.index(1)
        return last_speaker
    except ValueError:
        if 1 in is_active:
            last_speaker = is_active.index(1)
            return last_speaker
        return 0


def make_rating_str(time_dict):
    s = {k: time_dict[k] for k in sorted(time_dict, key=time_dict.get, reverse=True)}  # сортиєм по значеннях
    s = {k: v for k, v in s.items() if v}  # видаляем ключі з пистими значеннями

    time_tmp = list(map(lambda y: str(y + 1), s.keys()))  # формуєм строку з номерами мікрофонів
    str_rate = '{:0<4}'.format(''.join(time_tmp))  # добиваємо нулями

    return " ".join(str_rate)  # розділяєм пробілами і виводим


while True:

    # print(n)
    if int(gpio[5]):
        if is_active != olds:
            for n in range(len(is_active)):
                if is_active[n] and not flag_timer[n]:
                    Thread(target=speaking_thread, args=(n,)).start()

            last_speaker = check_lastXOR(is_active, olds) + 1
            olds = is_active[:]
            if is_active == [0, 0, 0, 0]:
                last_speaker = 0
            rating = str(last_speaker) + " " + make_rating_str(total_time)
            print("is_active- ", is_active, "rating -", rating)
        time.sleep(0.05)
    else:
        total_time = {a: 0 for a in range(4)}
        rating = "0 0 0 0 0"
        time.sleep(0.02)
