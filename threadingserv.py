#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import socket
import sys
from threading import Thread
from datetime import *
from time import sleep
import os
import subprocess
# todo: write config reloud class & implement
host = "localhost"
port = 888

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

last_active = dict.fromkeys(["symetrix", "atem", "rotator", "audiomixer", "rating"], datetime.now())
active_modules = dict.fromkeys(["symetrix", "atem", "rotator", "audiomixer", "rating"])
module_file_names = {"symetrix": "symetrixNEW.py", "audiomixer": "mixer.py", "rating": "rating.py",
                     "rotator": "rotator.py", "atem": "swictest.py", "sound": "mydelay.py"}
try:
    s.bind((host, port))
except socket.error:
    print("Binding filed")
    sys.exit()

print("Sock bounded")
s.listen(12)
print("Sock ready")
active_speakers = ""
ratings = ""
atem_in = "61"
atem_state = ""
gpio = " 0 0 0 0 0 0 0"
path = "/home/pi/FTP/producer/"


def run_modules(file_names):
    for name in file_names:
        sleep(1.5)
        subprocess.Popen("sudo " + path + file_names[name], shell=True)


def check_online():
    for key, value in last_active.items():
        # print(key, value)

        if datetime.now() - value > timedelta(seconds=5) and active_modules[key]:
            active_modules[key] = False
        elif datetime.now() - value < timedelta(seconds=5) and not active_modules[key]:
            active_modules[key] = True
        # todo: send alarm when some client offline

def set_online(name):
    last_active[name] = datetime.now()
    # print(last_active[name])


def audiomixer(payload):
    set_online("audiomixer")
    global gpio, atem_state
    #print("mixer -", payload)
    gpio = payload
    data = atem_state
    return data
    pass


def symetrix(payload):
    set_online("symetrix")
    global active_speakers
    #print("symetrix -", payload)
    active_speakers = payload
    pass


def atem(payload):
    set_online("atem")
    global atem_in, atem_state
    atem_state=payload
    # print("atem -", payload)
    data = atem_in
    return data
    pass


def rotator(payload):
    set_online("rotator")
    global atem_in
    #print("rotator -", payload)
    atem_in = payload
    data = "{0}{1}".format(ratings, gpio)
    #print('send to rotator', data,gpio)
    return data
    pass


def web(payload):
    data = "{0}{1}".format(ratings, gpio)
    return data
    pass


def manual(payload):
    #print(" manuall connected")
    data = '"symetrix": [{0}], "rating": "{1}", "atem": "{2}", "mixer": "{3}"'.format(active_speakers, ratings, atem_in, gpio)
    print(data)
    return data
    pass


def rating(payload):
    set_online("rating")
    global ratings
    #print("rating -", payload)
    ratings = payload
    data = "{0}{1}".format(active_speakers, gpio)
    return data
    pass


def clientthread(conn):

    print("Created thread")
    while True:
        try:
            sleep(0.001)
            data = conn.recv(1024).decode()
            reply = 'Ok'
            # print(data)
            if data[0:5] == "mixer": reply = audiomixer((data[5:]))
            if data[0:8] == "symetrix": symetrix((data[8:]))
            if data[0:6] == "rating": reply = rating(data[6:])
            if data[0:7] == "rotator": reply = rotator(data[7:])
            if data[0:4] == "atem": reply = atem(data[4:])
            if data[0:3] == "web": reply = web(data[3:])
            if data[0:6] == "manual": reply = manual(data[6:])
            if not data:
                break
            # print(reply)
            conn.sendall(reply.encode())
        except Exception as e:
            print("Connection error")
            print("Something went wrong - ", e)
            break
    conn.close()


def mainThread():
    while True:
        check_online()
        print(active_modules)
        sleep(1)

#run_modules(module_file_names) # Запуск модулів
check_online()
Thread(target=mainThread).start()
while True:
    conn, addr = s.accept()
    conn.settimeout(10)
    Thread(target=clientthread, args=(conn,)).start()

s.close()
