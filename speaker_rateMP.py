#!/usr/bin/env python3
# -*- coding: utf-8 -*-



from time import sleep, time
from socket import *
from threading import Thread
#import logging
import multiprocessing
olds = [0] * 4
is_active = [0] * 4
last_speaker = [0] * 4
last_speech = [time()] * 4
total_speech = [0] * 4
start_speak = [0.0] * 4


def sender(host="localhost"):  # Потік відправки данних
    while True:
        soc = socket(AF_INET, SOCK_STREAM)
        port = 888
        global is_active, rating, gpio
        try:
            soc.connect((host, port))
            while True:
                resive = soc.recv(1024).decode()
                prepare = resive.replace(',', '')
                if len(prepare) == 19:
                    is_active = prepare[0:8].split()
                    is_active = list(map(int, is_active))
                    gpio = prepare[8::2]

                is_active = list(map(int, is_active))
                is_active = check_active(is_active,gpio)
                sleep(0.1)

                message = 'rating' + rating
                print("-------------------------------send rating------------------------ ", message)
                soc.sendall(message.encode("utf8"))
                #logger.info("Rating "+ rating)
        except OSError:
            print("Connection to server error")
            sleep(2)  # 2 секунди до реконекту


def get_speek_time(is_active):
    for n in range(len(is_active)):

        if is_active[n] and not olds[n]:  # якшо змінився стан from 0 to 1 start speech
            start_speak[n] = time()
            print(n, ' ----- Start speak', time())
            olds[n] = 1

        elif is_active[n] and olds[n]:  # триває розмова 1 to 1
            last_speech[n] = time() - start_speak[n]
            start_speak[n] = time()
            print(n, " триває --- ", last_speech[n])
            total_speech[n + 1] += last_speech[n]
            #sleep(0.2)

        elif not is_active[n] and olds[n]:  # end spech from 1 to 0
            last_speech[n] = time() - start_speak[n]
            start_speak[n] = 0.0
            total_speech[n + 1] += last_speech[n]
            print(n, ' ----- Stop speak', time())

            olds[n] = 0
        elif is_active[n] and olds[n]:
            start_speak[n] = 0.0
    sleep(0.2)

def get_speek_counter(is_active, total_speech, olds, lock):   # Only for multiprocessing
    while True:
        lock.acquire()
        for n in range(len(is_active)):

            if is_active[n] and not olds[n]:  # якшо змінився стан from 0 to 1 start speech

                print(n, ' ----- Start speak',)
                total_speech[n + 1] += 1
                olds[n] = 1

            elif is_active[n] and olds[n]:  # триває розмова 1 to 1
                print(n, " триває --- ", total_speech[n])
                total_speech[n + 1] += 1


            elif not is_active[n] and olds[n]:  # end spech from 1 to 0
                total_speech[n + 1] += 1
                print(n, ' ----- Stop speak', total_speech[n])
                olds[n] = 0
        lock.release()
        print('total speach in MP ----', list(total_speech))
        sleep(0.5)
            # останній хто почав розмову


def check_last_spk(is_active): # find new speaker
    for n in range(len(is_active)):
        last_speaker[n] = is_active[n] - olds[n]


def check_last_spk_byTime(): # find new speaker by time
    print(" laste -- ", start_speak.index(max(start_speak)))


def check_lastXOR(is_active, olds): # find new speaker by XOR AND
    try:
        lock.acquire()
        b = list(map(lambda y, x: ((y ^ x) & x) & x, olds, is_active))
        lock.release()
        print('last -----', b)
        last_speaker = b.index(1) + 1
        return last_speaker
    except ValueError:
        if 1 in is_active:
            last_speaker = is_active.index(1) + 1
            return last_speaker
        return 0

def check_rating(timeRate, gpio):
    for n in timeRate:
        pass

def check_active(is_active, gpio): # clearing inactive mic
    active = list(map(int, gpio[:-2]))
    b = list(map(lambda y, x: y & x, active, is_active))
    return b

def test_rater():
    from random import randint

    for n in range(len(is_active)):
        is_active[n] = randint(0, 1)
    print(is_active)
    return is_active


if __name__ == '__main__':
    rating = "0 0 0 0 0"
    gpio = "000001"
    Thread(target=sender).start()
    lock = multiprocessing.Lock()
    olds = multiprocessing.Array("i",4)

    total_speech = multiprocessing.Array("i",4)
    spkTime = multiprocessing.Process(target=get_speek_counter, args=(is_active, total_speech, olds, lock))
    spkTime.start()
    while True:

        if int(gpio[5]):

            if is_active != olds:
                print("recived from symetrix --- ", is_active)
                last = check_lastXOR(is_active, olds)
                if last and last!=last_speaker: last_speaker = last
                #olds = list(is_active)
                # check_last_spk_byTime()
                #get_speek_time(is_active)
                tsDict = dict(enumerate(total_speech, 1))
                s = {k: tsDict[k] for k in sorted(tsDict, key=tsDict.get, reverse=True)} # sort dict by values
                s = {k: v for k, v in s.items() if v}  # del keys with empty values
                #rating = str([1 + (start_speak.index(max(start_speak)))] + list(s.keys())).strip('[]').replace(',', '') #  last by time
                timeRate = list(map(int,s.keys()))

                rating = str([last_speaker] + timeRate).strip('[]').replace(',', '')  # last speaker by XOR AND
                # print(type((start_speak.index(max(start_speak)))))
                print('strin -rating', rating)
                print("totall ----",list(total_speech))
                print("start speak dict",tsDict)
                print("gpio", gpio)
                print("is activee----",is_active,"olds----",list(olds))
                sleep(0.1)
        else:
            total_speech = dict.fromkeys(range(1, 5), 0)
            rating = "0 0 0 0 0"
            sleep(0.1)
