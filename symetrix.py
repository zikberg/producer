#!/usr/bin/env python3
#-*- coding: utf-8 -*-


from socket import *
import sys
from time import sleep, monotonic
from threading import Thread
from collections import deque


# def send_data

def push_init(sock, addr):
    controlers = ("6806 7809 \r",)

    pu = str.encode("PU 1 ")  # Global Push Enable
    sock.sendto(pu, addr)
    puc = str.encode("PUC 10000")  # Push Clear
    sock.sendto(puc, addr)
    pui = str.encode('PUI 200 \r')  # Set Push Interval
    sock.sendto(pui, addr)
    pue = str.encode("PUE 6808 7809\r")  # Push Enable
    sock.sendto(pue, addr)
    pue = str.encode("PUD 6809 7806 \r")  # Push Disable
    sock.sendto(pue, addr)


# def send_data


def get_push_levels(symetrix_ip):
    host = symetrix_ip
    port = 48630
    addr = (host, port)
    udp_socket = socket(AF_INET, SOCK_DGRAM)
    udp_socket.connect(addr)
    push_init(udp_socket, addr)
    meters = [0, 0]

    while True:

        level = udp_socket.recvfrom(1024)

        push_msg = ((bytes.decode(level[0])).rstrip())
        if len(push_msg) > 10:
            if push_msg[:6] == "#06808": meters[0] = int(push_msg[7:12])
            if push_msg[:6] == "#07808": meters[1] = int(push_msg[7:12])
            lvls_dict[symetrix_ip] = meters
    sleep(1)
    puc = str.encode('PU 0 \r')
    udp_socket.sendto(puc, addr)
    udp_socket.close()


def get_levels(sym_ip_list):  # опитуємо симетрікси і повертаємо список з вихідними рівнями
    levels_list = []
    for symetrix in sym_ip_list:
        host = symetrix
        port = 48630
        addr = (host, port)
        udp_socket = socket(AF_INET, SOCK_DGRAM)
        symetrix_comands = ['GS 6808 \r', 'GS 7808 \r']
        for comand in symetrix_comands:
            data = comand
            if not data:
                udp_socket.close()
                # sys.exit(1)
            # encode - перекодирует введенные данные в байты, decode - обратно
            data = str.encode(data)
            udp_socket.sendto(data, addr)
            level = udp_socket.recvfrom(1024)
            levels_list.append(int((bytes.decode(level[0])).rstrip()))

        udp_socket.close()

    return levels_list


def speak_detect(levels_list):  # піковий детектор, повертає список з статусами мікрофонів
    for out_number in range(len(levels_list)):
        LVLS[out_number].append(levels_list[out_number])
        if max(LVLS[out_number]) > threshold:
            active_speaker[out_number] = 1

        else:
            active_speaker[out_number] = 0


    pass


def make_lvls(lvls_dict):  # Формуємо єдиний список рівнів
    all_list = lvls_dict[symetrix_ip_list[0]]+lvls_dict[symetrix_ip_list[1]]
    # for key in lvls_dict:
    #    all_list += (lvls_dict[key])
    return all_list


def sender(host="localhost"):  # Потік відправки данних
    while True:
        soc = socket(AF_INET, SOCK_STREAM)
        port = 888
        try:
            soc.connect((host, port))
            send = 0.0
            old_speakers = ""
            while True:
                sleep(0.1)
                speakers = (str(active_speaker)).strip("[]")
                if old_speakers != speakers or monotonic()-send > 1:
                    old_speakers = speakers
                    message = 'symetrix' + speakers
                    print(message)
                    soc.sendall(message.encode("utf8"))
                    send = monotonic()
                else:
                    sleep(0.1)

        except OSError:
            print("Connection to server error")
            sleep(2)  # 2 секунди до реконекту





symetrix_ip_list = ["192.168.92.195", "192.168.92.23"]
lvls_dict = dict.fromkeys(symetrix_ip_list, [0, 0])
peak_detect_sensivity = 10
LVLS = [deque(maxlen=peak_detect_sensivity), deque(maxlen=peak_detect_sensivity), deque(maxlen=peak_detect_sensivity), deque(maxlen=peak_detect_sensivity)]  # список стеків з рівнями

active_speaker = [False] * 4
threshold = 28000  # Поріг детектора х=((48-дБу)*65535)/72

for ip in symetrix_ip_list:
    print(ip)
    Thread(target=get_push_levels, args=(ip,)).start()

Thread(target=sender).start()

while True:
    try:
        LVL = make_lvls(lvls_dict)  # get_levels(symetrix_ip_list)
        print("lvl", LVL)
        speak_detect(LVL)
        print(LVL)
        print(active_speaker)  # print(LVLS, sep='\n')
        sleep(0.1)
    except e as Exception:
        print(e)
