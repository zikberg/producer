import logging

def loginit(name):

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # create the logging file handler
    fh = logging.FileHandler(" logger.log")

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)

    # add handler to logger object
    logger.addHandler(fh)
    logger.info("Start script")
