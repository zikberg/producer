#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from socket import *
from time import sleep, monotonic
from threading import Thread
from collections import deque
import config
import client
import watchdog


def push_init(sock, addr):
    pu = str.encode("PU 1 ")  # Global Push Enable
    sock.sendto(pu, addr)
    puc = str.encode("PUC 10000")  # Push Clear
    sock.sendto(puc, addr)
    pui = str.encode('PUI 100 \r')  # Set Push Interval
    sock.sendto(pui, addr)
    pue = str.encode("PUE 6808 7809\r")  # Push Enable
    sock.sendto(pue, addr)
    pue = str.encode("PUD 6809 7806 \r")  # Push Disable
    sock.sendto(pue, addr)


def get_push_levels(symetrix_ip):  # повертає в словник {"ip":(lvl1, lvl2)}
    while True:
        host = symetrix_ip
        port = 48630
        addr = (host, port)
        udp_socket = socket(AF_INET, SOCK_DGRAM)
        try:
            udp_socket.connect(addr)
            print('conected')
            push_init(udp_socket, addr)
            meters = [0, 0]
            r_time = 0.0
            while True:
                level = udp_socket.recvfrom(1024)
                push_msg = ((bytes.decode(level[0])).rstrip())
                print(push_msg)
                if len(push_msg) > 10:
                    if push_msg[:6] == "#06808": meters[0] = int(push_msg[7:12])
                    if push_msg[:6] == "#07808": meters[1] = int(push_msg[7:12])
                    lvls_dict[symetrix_ip] = meters
                    # print(lvls_dict)
                    r_time = monotonic()

                if monotonic() - r_time > 0.2:
                    pur = str.encode("PUR 6808 7809\r")  # Push Refresh
                    udp_socket.sendto(pur, addr)
                    r_time = monotonic()
                    # todo: implement watchdog
                    # TODO: correct reconnect
        except OSError:
            print("Connection to symetrix " + symetrix_ip + " error")
            sleep(2)  # 2 секунди до реконекту


def speak_detect(levels_list):  # піковий детектор, повертає список з статусами мікрофонів
    global active_speaker, peak_levels_list
    for out_number in range(len(levels_list)):
        LVLS[out_number].append(levels_list[out_number])
        peak_levels_list[out_number] = max(LVLS[out_number])
        peak_levels_list[out_number] = max(LVLS[out_number])
        if max(LVLS[out_number]) > threshold:
            active_speaker[out_number] = 1

        else:
            active_speaker[out_number] = 0
    pass


def make_lvls(lvls_dict):  # Формуємо єдиний список рівнів

    all_list = [lvls_dict[mic["1"][0]][mic["1"][1]], lvls_dict[mic["2"][0]][mic["2"][1]],
                lvls_dict[mic["3"][0]][mic["3"][1]], lvls_dict[mic["4"][0]][mic["4"][1]]]
    return all_list


def on_message(c):
    pass


def on_newdata(c):
    pass


def sender(c):
    speakers = (str(active_speaker)).strip("[]")
    c.to_send = speakers


c = client.Client("symetrix")
c.run()
c.on_message = on_message
c.on_newmsg = on_newdata
c.send_handler = sender

symetrix_ip_list = config.symetrix_ip_list
mic = config.symetrix_mic_in
lvls_dict = dict.fromkeys(symetrix_ip_list, [0, 0])
peak_detect_sensivity = 20
LVLS = [deque(maxlen=peak_detect_sensivity), deque(maxlen=peak_detect_sensivity),
        deque(maxlen=peak_detect_sensivity), deque(maxlen=peak_detect_sensivity)]  # список стеків з рівнями

active_speaker = [False] * 4
threshold = config.threshold  # Поріг детектора х=((48-дБу)*65535)/72

peak_levels_list = [False] * 4

for ip in symetrix_ip_list:
    print(ip)
    Thread(target=get_push_levels, args=(ip,)).start()

Thread(target=sender).start()

while True:
    try:
        LVL = make_lvls(lvls_dict)  # get_levels(symetrix_ip_list)
        # print("lvl", LVL)
        speak_detect(LVL)
        # print(LVLS)
        print(active_speaker)  # print(LVLS, sep='\n')
        print("Pik levels - ", peak_levels_list)
        sleep(0.05)
    except Exception as e:
        print(e)
