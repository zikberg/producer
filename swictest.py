#!/usr/bin/env python3
#  -*- coding: utf-8 -*-

from atemlib import *
import config
import time
import client
import watchdog

send = 0.0
answer = 0.0


def inputWach(atem):
    global atem_state, comand
    print("on air --", atem.state['program'][0])
    atem_state = atem.state['program'][0]
    if atem_state == comand[1]:
        atem_wd.stop()


def tallyWach(atem):
    pass


def cmdOkWach(atem):
    print("-------------------- Comand accepted ---------------------------------")


def cmdErrorWach(atem):
    print(">>>>>>>>>>>>>>>>>>>>>>>error detected>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    # time.sleep(1)
    # raise TimeoutError("Atem timeout")


def receiveWach(atem):
    print("Packet already received")
    raise TimeoutError
    pass


def self_test(comand):
    import random
    time.sleep(5)
    while True:
        time.sleep(2)
        comand[0] = 1
        comand[1] = random.randint(1, 6)
        comand[2] = 0
        print('Test command -  ', list(comand))


def on_message_c(c):
    switch_wd.reset()
    pass


def on_newdata(c):
    global comand
    resive = c.received
    comand[0] = 1
    comand[1] = int(resive[0])
    comand[2] = int(resive[1])


def sender(c):
    c.to_send = atem_state
    pass


def atem_timeout():
    # watchdog timeout
    print("Atem dont confirm corect switching")
    # todo: find best way to reconnect
    pass


def switch_timeout():
    global comand
    print("No data from server switch to player")
    comand = [1, 6, 1]

    pass

comand = [0, 0, 0]

atem_state = ""

if __name__ == "__main__":

    # p = multiprocessing.Process(target=sender, args=(comand, lock))
    # p.start()
    # t = multiprocessing.Process(target=self_test, args=(comand,))
    # t.start()

    while True:
        try:
            a = Atem(config.address)
            a.connectToSwitcher()
            a.pgmInputHandler = inputWach
            a.cmdAcceptedHandler = cmdOkWach
            a.cmdErrorHandler = cmdErrorWach

            c = client.Client("atem")
            c.run()
            c.on_message = on_message_c
            c.on_newmsg = on_newdata
            c.send_handler = sender

            atem_wd = watchdog.Watchdog(2, atem_timeout)

            switch_wd = watchdog.Watchdog(10, switch_timeout)
            switch_wd.start()

            print("Connect to switch")
            time.sleep(0.1)
            comand = [0, 0, 0]
            while True:

                a.waitForPacket()

                # print("comand from sender", comand)
                # lock.acquire()

                if comand[0]:
                    print("New cmd ---", comand[0])
                    print("Inpu num --", comand[1])
                    print("Auto cmd --", comand[2])
                    if comand[2]:
                        a.sendCommand(b'\x43\x50\x76\x49\x00\x10', struct.pack(">h", comand[1]))
                        a.sendCommand(b'\x44\x41\x75\x74\x00\x8f', b'\x1f\x76')
                        comand[2] = 0
                    # send = time.monotonic()
                    # if send - answer > 2.0: raise TimeoutError("Atem timeout")
                    else:
                        a.sendCommand(b'\x43\x50\x67\x49\x00\x10', struct.pack(">h", comand[1]))

                    # a.sendCommand(b'\x44\x41\x75\x74\x00\x8f', b'\x1f\x76')
                    atem_wd.start()
                    comand[0] = 0
                # lock.release()
        except TimeoutError as exp:
            print("Error", exp)

            a.disconnectFromSwitcher()
            del a
            time.sleep(4)
            print("Reconecting.....")
        # except Exception as err:
        # print("Everything went wrong - ", err)

'''

CPgI=b'\x43\x50\x67\x49\x00\x10'
CPvI=b'\x43\x50\x76\x49\x00\x10'
DAut=b'\x44\x41\x75\x74\x00\x8f\x1f\x76'
        44:41:75:74:00:8f:1f:76
44:41:75:74:00:8f:1f:76
:44:41:75:74:00:b9:0c:bb
44:41:75:74:00:b9:0c:bb

'''
