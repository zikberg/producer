#!/usr/bin/env python3
# -*- coding: utf-8 -*-



from time import sleep, time
from socket import *
from threading import Thread
import logging
olds = [0] * 4
is_active = [0] * 4
last_speaker = [0] * 4
last_speech = [time()] * 4
total_speech = dict.fromkeys(range(1, 5), 0.0)
start_speak = [0.0] * 4


def sender(host="localhost"):  # Потік відправки данних
    while True:
        soc = socket(AF_INET, SOCK_STREAM)
        port = 888
        global is_active, rating, gpio
        try:
            soc.connect((host, port))
            while True:
                resive = soc.recv(1024).decode()
                prepare = resive.replace(',', '')
                if len(prepare) == 19:
                    is_active = prepare[0:8].split()
                    is_active = list(map(int, is_active))
                    gpio = prepare[8::2]

                is_active = list(map(int, is_active))
                is_active = check_active(is_active,gpio)
                sleep(0.1)

                message = 'rating' + rating
                print("-------------------------------send rating------------------------ ", message)
                soc.sendall(message.encode("utf8"))
                logger.info("Rating "+ rating)
        except OSError:
            print("Connection to server error")
            sleep(2)  # 2 секунди до реконекту


def get_speek_time(is_active):
    for n in range(len(is_active)):

        if is_active[n] and not olds[n]:  # якшо змінився стан from 0 to 1 start speech
            start_speak[n] = time()
            print(n, ' ----- Start speak', time())
            olds[n] = 1

        elif is_active[n] and olds[n]:  # триває розмова 1 to 1
            last_speech[n] = time() - start_speak[n]
            start_speak[n] = time()
            print(n, " триває --- ", last_speech[n])
            total_speech[n + 1] += last_speech[n]
            #sleep(0.2)

        elif not is_active[n] and olds[n]:  # end spech from 1 to 0
            last_speech[n] = time() - start_speak[n]
            start_speak[n] = 0.0
            total_speech[n + 1] += last_speech[n]
            print(n, ' ----- Stop speak', time())

            olds[n] = 0
        elif is_active[n] and olds[n]:
            start_speak[n] = 0.0
    sleep(0.2)


        # останній хто почав розмову


def check_last_spk(is_active):
    for n in range(len(is_active)):
        last_speaker[n] = is_active[n] - olds[n]


def check_last_spk_byTime():
    print(" laste -- ", start_speak.index(max(start_speak)))


def check_lastXOR(is_active, olds):
    try:
        b = list(map(lambda y, x: ((y ^ x) & x) & x, olds, is_active))
        print('b___', b)
        last_speaker = b.index(1) + 1
        return last_speaker
    except ValueError:
        return 0

def check_active(is_active, gpio):
    active = list(map(int, gpio[:-2]))
    b = list(map(lambda y, x: y & x, active, is_active))
    return b

def test_rater():
    from random import randint

    for n in range(len(is_active)):
        is_active[n] = randint(0, 1)
    print(is_active)
    return is_active


'''
test_rater()
k=0
while k<50:
    k+=1
    test_rater()
    print("old>>>", olds)
    print("new>>>", is_active)

    last=0
    if is_active != olds:
        last = check_lastXOR(is_active, olds)
        print("last", last)
        #olds = list(is_active)
    else:

        print("old=new")
    get_speek_time(is_active)
    sleep(1)
    s = {k: total_speech[k] for k in sorted(total_speech, key=total_speech.get, reverse=True)}
    rating = str([last] + list(s.keys())).strip('[]').replace(',', '')
    print(rating)
#mydict = total_speech

#dict = {k:v for k,v in (x.split(':') for x in list) }

#for k, v in s:
#    print(k)

s = {k: total_speech[k] for k in sorted(total_speech, key=total_speech.get, reverse=True)}
rating = str([1 + (start_speak.index(max(start_speak)))] + list(s.keys())).strip('[]').replace(',', '')
#rating = [start_speak.index(max(start_speak))] + list(s.keys())

print("rating", rating)
'''

logger = logging.getLogger("speaker rate")
logger.setLevel(logging.DEBUG)

# create the logging file handler
fh = logging.FileHandler("logger.log")

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)

# add handler to logger object
logger.addHandler(fh)
logger.info("Start script")

#p = multiprocessing.Process(target=sender, args=(comand, lock))
#p.start()

rating = "0 0 0 0 0"
gpio = "000000"
Thread(target=sender).start()
while True:

    if int(gpio[5]):

        if is_active != olds:
            last = check_lastXOR(is_active, olds)
            if last and last!=last_speaker: last_speaker = last
            #olds = list(is_active)
            # check_last_spk_byTime()
            get_speek_time(is_active)
            s = {k: total_speech[k] for k in sorted(total_speech, key=total_speech.get, reverse=True)}

            #rating = str([1 + (start_speak.index(max(start_speak)))] + list(s.keys())).strip('[]').replace(',', '') #  laast by time
            rating = str([last_speaker] + list(s.keys())).strip('[]').replace(',', '')  # last speaker by XOR AND
            # print(type((start_speak.index(max(start_speak)))))
            print('strin -rating', rating)
            print("totall ----",total_speech)
            print("start speak list", start_speak)
            sleep(0.1)
    else:
        total_speech = dict.fromkeys(range(1, 5), 0.0)
        rating = "0 0 0 0 0"
        sleep(0.1)
