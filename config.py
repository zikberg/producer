address = '192.168.92.230'  # Atem switcher IP

symetrix_ip_list = ["192.168.82.201", "192.168.82.202"]  # Symetrix 2x ip

symetrix_mic_in = {"1": [symetrix_ip_list[1], 1],  # associate symetrix input with mixer line
                   "2": [symetrix_ip_list[0], 1],
                   "3": [symetrix_ip_list[1], 0],
                   "4": [symetrix_ip_list[0], 0]}

mic_to_cam = {"0": "0", "1": "2", "2": "1", "3": "3", "4": "4", "room": "5", "player": "6", "last":"5"}  # associate switch input with mic (last


on_air_alg = [["room", 3], ["last", 3], ["last",4], ["last", 3]]
off_air_alg = [["room", 120], ["player", "S", "auto"]]

threshold = 32000  # active mic level threshold х=((48-дБу)*65535)/72
