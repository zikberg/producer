from threading import Thread
import socket
from time import sleep


class Client():
    def __init__(self, name):
        self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.name = name
        self.host = "localhost"
        self.port = 888
        self.received = ""
        self.to_send = ""
        self.send_handler = None
        self.sleep_time = 0.1
        self.on_message = None
        self.on_newmsg = None
        self.old_data = ""

    def establish_connection(self):
        self.soc.connect((self.host, self.port))
        self.soc.settimeout(2)

    def send_data(self):
        self.send_handler(self)
        message = self.name + str(self.to_send)
        self.soc.send(message.encode("utf8"))
        #print("Send to router", message)

    def receive(self):
        self.received = self.soc.recv(2048).decode()
        #print("Received from router ", self.received)
        if self.received:
            self.on_message(self)
            self.check_new()

    def main_thread(self):
        while True:
            try:
                self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.establish_connection()
                while True:
                    try:

                        self.send_data()
                        self.receive()
                        sleep(self.sleep_time)
                    except socket.timeout:
                        print("Time out exception")
                        sleep(2)  # 2 секунди до реконекту
            except socket.error as e:
                print("Some socket error ", e)
                self.soc.close()
                sleep(2)  # 2 секунди до реконекту
            except OSError:
                print("Connection to server error")
                sleep(2)  # 2 секунди до реконекту

    def run(self):
        Thread(target=self.main_thread).start()

    def check_new(self):
        if self.received != self.old_data:
            self.old_data = self.received
            self.on_newmsg(self)
