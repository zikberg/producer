#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from time import sleep

import config
import client



def show_last(tmin, tmax):
    global data, atem_in, previous
    if int(data[0]):
        atem_in = mic_to_cam[str(data[0])]
        print("last data to atem - ", atem_in)
        if previous == atem_in:
            tmin = 0
        sleep(tmin)
        t = 0
        while t < tmax:
            if mic_to_cam[str(data[0])] != atem_in:
                break
            sleep(0.2)
            t += 0.2
        previous = atem_in


def show_cam(cam, time):
    global atem_in, previous, data
    atem_in = mic_to_cam[cam]
    if int(data[10]) and not int(data[11]):  # on air mode

        print("cam data to atem - ", atem_in)
        sleep(time)
        previous = atem_in
    else:
        t = 0
        while t < time:
            if int(data[10]):
                break
            if int(data[11]):
                break
            sleep(0.1)
            t += 0.1


def select_mode(on_air, off_producer):
    if on_air and not off_producer:
        alg = config.on_air_alg
        # print("-------------------!!!ON AIR!!!---------------   last: ", last)

    else:
        alg = config.off_air_alg

        # print("-------------------!!!OFF AIR!!!---------------")
    if off_producer:
        alg = [["player", "S", "auto"]]
    return alg


def set_auto(cam):
    global cmd
    if len(cam) == 3:
        if cam[2] == "auto":
            cmd = "1"
    else:
        cmd = "0"


def on_message(c):
    pass


def on_newdata(c):
    global data
    data = c.received.replace(' ', '')

    print("new data resivd", data)

def sender(c):
    c.to_send=atem_in+cmd


c = client.Client("rotator")
c.run()
c.on_message = on_message
c.on_newmsg = on_newdata
c.send_handler = sender

data = "131420000000"
send = 0
last = 0
previous = 0
cmd = "0"
old_atem_in = "0"
mic_to_cam = config.mic_to_cam

if __name__ == "__main__":

    while True:

        print("data - ", data)
        tally = int(data[10])
        off_producer = int(data[11])
        alg = select_mode(tally, off_producer)
        if tally and not off_producer:
            last = data[0]
            print('selected alg -', alg)
            for cam in alg:
                set_auto(cam)
                if cam[0] == "last":
                    show_last(2, cam[1])
                else:
                    show_cam(cam[0], cam[1])
        elif not tally and not off_producer:
            for cam in alg:
                set_auto(cam)
                duration = cam[1]
                if duration == "S":
                    atem_in = str(mic_to_cam[str(cam[0])])
                    print("player")
                    while not int(data[10]):
                        sleep(0.01)
                    break
                else:
                    show_cam(cam[0], cam[1])
        elif off_producer:
            atem_in = "6"
            pass
