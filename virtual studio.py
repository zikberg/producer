import random
from socket import *
from time import sleep, monotonic
from threading import Thread


def mic_sender(host="localhost"):  # Потік відправки данних
    while True:
        global active_speaker
        soc = socket(AF_INET, SOCK_STREAM)
        port = 888
        try:
            soc.connect((host, port))
            send = 0.0
            old_speakers = ""
            while True:
                sleep(0.1)
                speakers = (str(active_speaker)).strip("[]")
                if old_speakers != speakers or monotonic() - send > 1:
                    old_speakers = speakers
                    message = 'symetrix' + speakers
                    print(message)
                    soc.sendall(message.encode("utf8"))
                    send = monotonic()
                else:
                    sleep(0.1)

        except OSError:
            print("Connection to server error")
            sleep(2)  # 2 секунди до реконекту


def mix_sender(host="localhost"):  # Потік відправки данних
    while True:
        soc = socket(AF_INET, SOCK_STREAM)
        port = 888
        try:
            soc.connect((host, port))

            old_s = ""
            while True:
                sleep(0.2)
                if old_s != s:
                    old_s = s
                    message = 'mixer' + " " + s
                    print(message)
                    soc.sendall(message.encode("utf8"))
                else:
                    sleep(1)
                    old_s = ""
        except OSError:
            print("Connection to server error")
            sleep(2)  # 2 секунди до реконекту


def random_mixer(gpio):
    for n in range(len(gpio) - 1):
        gpio[n] = random.randint(0, 1)
    return gpio


def make_gpio_str(gpio):
    data = ""
    for n in gpio:
        data += str(n) + " "
    return data[:-1]


if __name__ == "__main__":
    active_speaker = [0, 0, 0, 0]
    gpio = [0] * 6
    s = "0 0 0 0 0 0"
    Thread(target=mic_sender).start()
    Thread(target=mix_sender).start()
    while True:

        delta_time_mic = random.randint(2, 9)
        delta_time_on_air = random.randint(15, 60)
        print(delta_time_mic)
        sleep(delta_time_mic)
        for n in range(len(active_speaker)):
            active_speaker[n] = random.randint(0, 1)
        print(active_speaker)
        gpio = random_mixer(gpio)
        gpio[5] = 1
        s = make_gpio_str(gpio)
        print("gpio = ", s)

"""
symetrix: 0, 0, 0, 0

mixer:  0 0 0 0 0 0
"""
